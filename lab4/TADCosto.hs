-- TAD Costo

module TADCosto(Costo, finitoC, infinitoC, masC) where

import TADNatural

-- CONSTRUCTORES
--El orden en el cual estan definidos los constructores,y ord determinan que Finito<Infinito
data Costo = Finito Natural 
           | Infinito
           deriving (Eq, Ord, Show)

-- CONSTRUCTORES

finitoC = Finito
infinitoC = Infinito

-- OPERACIONES

masC :: Costo -> Costo -> Costo

-- ECUACIONES

Finito n `masC` Finito m = Finito (n `mas` m)
_ `masC` _ = Infinito



