#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../stack.c"
#include "../stack.h"

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Usage: ./reverse <string-to-reverse>\n");
        exit(EXIT_FAILURE);
    }
    char *str = argv[1];
    int len=strlen(str);

    //Creo pila vacia
    _stack *q=NULL;
    
    for(int i=0;i<len;i++){
        stack_elem_t e=str[i];
        q=stack_push(q,e);
    }
    int size=stack_size(q);
    for(int i=0;i<size;i++){
        str[i]=stack_top(q);
        q=stack_pop(q);
        
        
    }
    printf("%s\n", str);
    
   
    //Iterar sobre el string (char *) de izquierda a derecha, insertando los
    //caracteres uno por uno en la pila
    
}
