#include <stdlib.h>
#include "assert.h"
#include "stack.h"

typedef struct _stack {
    stack_elem_t value;
    stack_t next;
}_stack;


stack_t stack_empty(){
    _stack *st=NULL;
    
    return st;
}

stack_t stack_push(stack_t s, stack_elem_t elem){
    _stack *q=NULL;
     q=calloc(1,sizeof(struct _stack));
    (*q).value = elem;

    (*q).next = s;
    s = q;
    return s;
}

stack_t stack_pop(stack_t s) {
    _stack *q=NULL;
    q = s;
    s = (*s).next;
    free(q);

    return s;
}

unsigned int stack_size(stack_t s){
    _stack *p=NULL;
    unsigned int count=0;
    p=s;
    while(p != NULL){
        count++;
        p=(*p).next;
    }
    return count;
}

stack_elem_t stack_top(stack_t s){
    
    assert( !(stack_is_empty(s)) );

    stack_elem_t e = (*s).value;
    return e;
}

bool stack_is_empty(stack_t s){
    bool b =(s==NULL);
    return b;
}

stack_elem_t *stack_to_array(stack_t s){
    unsigned int size = stack_size(s);

    if(s == NULL) {
        return NULL;
    } 
    else {
        stack_elem_t* array;
        array = (stack_elem_t*)calloc(size,sizeof(stack_elem_t));

        int i=size-1;
        while(s != NULL && i>=0) {
            array[i] = (*s).value;
            s = (*s).next;
            i--;
        }
        return array;
    }        
}

stack_t stack_destroy(stack_t s){
        _stack *q=NULL;
        
        while(s !=NULL){
            q=s;
            s=(*s).next;
            free(q);
        }
        
        return s;   
}
