#include <assert.h>
#include <stdlib.h>
#include "map.h"

struct _node_t {
    map_t left;
    map_t right;
    key_t key;
    value_t value;
};

map_t map_empty() {
    return NULL;
}

static map_t create_node(key_t key, value_t value, map_t left, map_t right) {
    map_t node = NULL;
    node=calloc(1,sizeof(struct _node_t));
    node->key=key;
    node->value=value;
    node->left=left;
    node->right=right;

    return (node);
}

static map_t free_node(map_t node) {
   free(node->right);
    free(node->left);
    free(node);
    
    return (NULL);
}

map_t map_put(map_t map, key_t key, value_t value) {
    assert(key != NULL);
    assert(value != NULL);
    map_t node = map;
    if(node==NULL){
        node=create_node(key,value,NULL,NULL);
    }
    else if(string_eq(node->key,key)){
         node =create_node(node->key,value,node->left,node->right);
    }
    else if(string_less(key,node->key)){
        node->left= map_put(node->left,key,value);
    }
    else{
        node->right=map_put(node->right,key,value);
    }
    
   
    return (node);
}

value_t map_get(map_t map, key_t key) {
    assert(key != NULL);
    value_t value = NULL;
     if(!map_contains(map,key)){
        value=NULL;
     }
     if(string_eq(map->key, key)){
         value=map->value;
     }
     else if(map_contains(map->right,key)){
     value=map_get(map->right,key);
     }
     else{
     value=map_get(map->left,key);
     }
    return (value);
}

bool map_contains(map_t map, key_t key) {
    assert(key != NULL);
    bool b1, b2= false;
    if(map==NULL){
        return false;
    }
    if(string_eq(map->key,key)){
        return true;
    }
    b1=map_contains(map->right,key);
    b2=map_contains(map->left, key);

    return (b1||b2);
}
map_t minvalue(map_t map){
    map_t aux=map;
    while(aux && aux->left!=NULL){
        aux=aux->left;
    }
    return aux;
}

map_t map_remove(map_t map, key_t key) {
    map_t result = map;
    if(!map_contains(map,key)) {
        printf("no existe la key");
    }

    if (map != NULL) {
        // Si la key es menor , seguimos buscando por izq
        if(string_less(key,map->key)){
            map->left = map_remove(map->left,key);
        }
        // Si la key es mayor , seguimos buscando por der
        else if(!string_less(key,map->key) && !string_eq(key,map->key)){
            map->right = map_remove(map->right,key);
        }
        //Si ya encontramos la key
        else {
            //1er caso: TIENE UN HIJO O NO TIENE HIJOS
            if(map->left==NULL){
                result = map->right;
                free_node(map);
                return result;
            }
            else if(map->right == NULL) {
                result = map->left;
                free_node(map);
                return result;
            }

            //2do caso : TIENE DOS HIJOS
            result = minvalue(map->right);
            map->key=result->key;
            map->right=map_remove(map->right, result->key);

        }
    }
    
    return (map);
}

map_t map_destroy(map_t map) {
    if(map == NULL) {
        return NULL;
    } 
    else {
        map_destroy(map->left);
        map_destroy(map->right);
        free(map);
        map = NULL;
    }
    return map;
}

void map_dump(map_t map, FILE *file) {
    if (map != NULL) {
        map_dump(map->left, file);
        key_dump(map->key, file);
        fprintf(file, ": ");
        value_dump(map->value, file);
        fprintf(file, "\n");
        map_dump(map->right, file);
    }
}
bool is_ABB(map_t map){
    assert(map!=NULL );//corregir
    bool b1, b2 = true;
        
     if(map->right!=NULL && map->left !=NULL){       
            return (!string_less(map->right->key,map->key)) && string_less(map->left->key,map->key);
        
     }
     else if(map->right!=NULL && map->left==NULL){
        return !string_less(map->right->key,map->key);
     }
     else if(map->right==NULL && map->left!=NULL){
         return string_less(map->left->key,map->key);
     }
     else{
         return true;
     }
     b1=is_ABB(map->right);
     b2=is_ABB(map->left);
     return(b1 && b2);
        
}
