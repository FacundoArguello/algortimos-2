#ifndef _WEATHER_UTILS_H
#define _WEATHER_UTILS_H
#include <stdio.h>
#include <stdbool.h>
#include "weather.h"
#include "array_helpers.h"

int minTemp(WeatherTable a);
void maxTemp(WeatherTable b,int output[]);
void maxPrec(WeatherTable array,int output[]);
#endif
