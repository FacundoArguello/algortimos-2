#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "weather_utils.h"
#include "weather.h"
#include "array_helpers.h"

int minTemp(WeatherTable array) {
	Weather weather = array[0][0][0];
	int minT = weather._min_temp;

	for(int a=0;a<=36;a++) {
	   for(int m=0;m<=11;m++){
	      for(int d=0;d<=27;d++){
		 if ( minT > (array[a][m][d])._min_temp ) {
		 	minT = (array[a][m][d])._min_temp;
	    	}
	 
	      }
        }
		
	}

	return minT;
}
void maxTemp(WeatherTable array,int output[]) {
	
        for(int a=0;a<=36;a++) {
            Weather weather = array[a][0][0];
	       	int maxT = weather._max_temp;
		   for(int m=0;m<=11;m++){
		      for(int d=0;d<=27;d++){
				if ( maxT < (array[a][m][d])._max_temp ) {
				 	maxT = (array[a][m][d])._max_temp;
			    }
		 
		      }
	        }
	    output[a]=maxT;
		}
}

void maxPrec(WeatherTable array,int output[]) {

		int aux[MONTHS];

		for(int a=0;a<=36;a++) {
		   for(int m=0;m<=11;m++){
		   	  Weather weather = array[a][m][0];
	       	  unsigned int maxPrec = weather._pressure;
		      for(int d=0;d<=27;d++){
				if ( maxPrec < (array[a][m][d])._pressure ) {
				 	maxPrec = (array[a][m][d])._pressure;
			    }
		 
		      }
		      aux[m] = maxPrec; //Mes con cada precipitación más alta.
	        }
	    	int mayor,mesMayor;
	    	mayor = aux[0];
	    	for(int k=0;k<=11;k++){
	    		if(mayor < aux[k]){
	    			mayor=aux[k];
	    			mesMayor = k;
	    		}
	    	}
	    output[a] = mesMayor;
		}
}

