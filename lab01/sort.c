#include <stdio.h>
#include "sort.h"

//Auxiliares abajo 1 2 3

void swap(int a[], unsigned int i, unsigned int j)
{
	unsigned int tmp;
     	tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
} 

unsigned int min_pos_from(int a[], unsigned int length, unsigned int i)
{
        unsigned int minp = i;

	for (unsigned int j=i+1; j< length; j++)
        {
          if (a[j] < a[minp])
             {
              minp = j;
	     }
 
	}
       return minp;
}



bool array_is_sorted(int array[], unsigned int length)
{
	bool ord = true;
	for(unsigned int i=0;i<length-1;i++) {
		if(array[i] > array[i+1]) {
			ord = false;		
		}
	}	
	
	printf("array=%p length=%u\n", (void*)array, length);
	return ord;
}

//insertion sort
void insert(int a[],unsigned int i) 
{
       for(unsigned int j=i; j>0 && a[j]<a[j-1];j--) 
       {
	swap(a,j-1,j);
       }	
}

void insertion_sort(int array[], unsigned int length)
{
	for(unsigned int i=1;i<length;i++)
	{
		insert(array,i);
	}    	
	

	printf("array=%p length=%u\n", (void*)array, length);
}




//selection sort

void selection_sort(int array[], unsigned int length)
{
	unsigned int minp;
        
	for(unsigned int i=0;i<length;i++)
        {
        	minp= min_pos_from(array,length,i);
		swap(array,i,minp); 

	}	



	printf("array=%p length=%u\n", (void*)array, length);
}

//quick sort

unsigned int partition(int a[], unsigned int lft, unsigned int rgt)
{
  unsigned int i, j;
  unsigned int piv;
  piv = lft;
  i= lft + 1;
  j= rgt - 1;
  
  while(i<=j) {
       
         if (a[i]<=a[piv]){
              i++;
 
         } 

         else if (a[j]>= a[piv]){
                 j--;         
	 }

         else if(a[i] > a[piv] && a[j] < a[piv]){
                 swap(a,i,j); 
                 i++;
 		 j--;
 	 }
   }

   swap(a,piv,j);  
   piv=j;
   return piv;
 
}
void quick_sort_rec(int a[],unsigned int lft,unsigned int rgt) 
{	
	unsigned int piv;
	if(rgt > lft+ 1) {
		piv= partition(a,lft,rgt);
		quick_sort_rec(a,lft,piv);
		quick_sort_rec(a,piv+1,rgt);
			
	}
}

void quick_sort(int array[], unsigned int length)
{
		
	quick_sort_rec(array,0,length);
	printf("array=%p length=%u\n", (void*)array, length);
}

//Si llamamos en el primer qsr con piv-1, se romperia el if.



