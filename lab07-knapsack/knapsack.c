#include <stdlib.h>
#include <stdbool.h>
#include "knapsack.h"

 unsigned int max(unsigned int a, unsigned int b){
     if(a>=b){
         return a;
     }
     else{
         return b;
     }
 }

value_t knapsack_backtracking(item_t *items, unsigned int array_length, weight_t max_weight){
    value_t val;
    unsigned int a=array_length;
    unsigned int m=max_weight;
    if(m==0) {
       val=0;
    } 
    else if(m>0 && a==0){
        val=0;
    }
    else if(item_weight(items[a-1]) > m && m > 0 && a>0){
       val= knapsack_backtracking(items,a - 1u,m);
    }
    else{
       val= max(knapsack_backtracking(items,a - 1u,m),
             item_value(items[a-1]) + knapsack_backtracking(items,a-1u, m - item_weight(items[a-1])));

    }
    return val;
 }
 value_t knapsack_dynamic(item_t *items, unsigned array_length, weight_t max_weight){
     
     unsigned int n=array_length;
     unsigned int w=max_weight+1;
     unsigned int **moch;
     unsigned int f;
     moch = (unsigned int **)malloc (n*sizeof(unsigned int *));

      for (unsigned int i=0;i<n;i++){
        moch[i] = (unsigned int *) malloc (w*sizeof(unsigned int));//arreglar punteros i<=n
      }
     
     for (unsigned int i=0;i<n;i++){
         moch[i][0]=0;
     }
     for (unsigned int j=1;j<w;j++){
         moch[0][j]=0;
     }
     for(unsigned int i=1;i<n;i++){
         if(i == array_length-1u){
             f=array_length-1u;
         }
         else if (i==1){
             f=0;
         }
         else {
             f=i;
         }

         for(unsigned int j=1; j<w;j++){
             if(item_weight(items[f])>j){
                moch[i][j]=moch[i-1][j]; 
             }
             else{
                 moch[i][j]=max(moch[i-1][j],item_value(items[f]) + moch[i-1][j-item_weight(items[f])]);
             }
         }
     }

    return moch[n-1][w-1];
 }
 value_t knapsack_dynamic_selection(item_t *items, bool *selected, unsigned int array_length,weight_t max_weight){
     unsigned int n=array_length;
     unsigned int w=max_weight;
     unsigned int moch[n][w];
     for(unsigned int i=0;i<n;i++){
         selected[i]=false;
     }
     
     for (unsigned int i=0;i<n;i++){
         moch[i][0]=0;
     }
     for (unsigned int j=1;j<w;j++){
         moch[0][j]=0;
     }
     for(unsigned int i=1;i<n;i++){
         for(unsigned int j=1; j<w;j++){
             if(item_weight(items[i-1])>j){
                moch[i][j]=moch[i-1][j]; 
             }
             else{
                 selected[i]=true;
                 moch[i][j]=max(moch[i-1][j],item_value(items[i-1]) + moch[i-1][j-item_weight(items[i-1])]);
             }
         }
     }

    return moch[n-1][w-1];
 }
